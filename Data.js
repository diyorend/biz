export const NewsInfoData = {
  src: require('./assets/imgs/newsImg1.png'),
  title: "Qirg'izistonda yana siyosiy inqiroz: Atamboyev nega taslim bo'ldi?",
  desc: ` — O'zbekistonga qaytishni xohlagan xorijdagi fuqarolarning katta qismi Rossiya Federatsiyasi hissasiga to'g'ri keladi. E'lon qilingan parvozlar ro'yxatida Rossiyaning 8 ta shaharlaridan 11-24 may oralig'ida charter reyslar amalga oshiriladi. Albatta, bu 8 ta aviareyslar orqali barcha fuqarolarni olib kelishning imkoniyati yo'q. Buni fuqarolar to'g'ri tushunishlari lozim. Konsulliklar va elchixonalarimiz orqali qaytish istagida bo'lgan fuqarolarning ro'yxati shakllantirib borilyapti. Ro'yxatdan o'tgan barcha fuqarolarni charter reyslar orqali olib kelishga harakat qilinadi. Ulardan bizni to'g'ri tushungan holda sabrli bo'lishni so'raymiz.  

  —Qozog'iston chegarasida yuzlab o'zbekistonliklar o'z avtomashinalarida O'zbekistonga o'tish uchun ruxsat berilishini  kutmoqda. Transport vazirligi bu masalada qo'shnilar bilan aloqa  o'rnatdimi? 
  
  — Qozog'iston va Rossiya chegarasida taksi xizmatini ko'rsatuvchi bir necha yuzlab fuqarolarimiz to'planib qolgan va buni ijtimoiy tarmoqlarda videomurojaatlar orqali chiqishlarda bevosita guvohi bo'ldik. Bu bo'yicha Transport vazirligi tegishli idoralar bilan hamkorlikda ushbu masalaga yechim topish ustida ishlayapti. Bir jihatni alohida ta'kidlash kerak: ushbu fuqarolar Rossiya va Qozog'istonda belgilangan karantin talablarini qo'pol ravishda buzib harakatlanishmoqda. Qaytaraman, bu borada muzokaralar boshlandi. 
  
  —Rossiyada moddiy jihatdan qiyin ahvolga tushib qolgan va aviachipta uchun puli qolmagan vatandoshlarningO'zbekistonga qaytib olishlari uchun vazirlik tomonidan ko'mak beriladimi? 
  
  — O'zbekistonga qaytish istagida bo'lgan, ammo aviachipta xarid qilishga imkoniyati bo'lmagan fuqarolar masalasi individual tarzda o'rganiladi va imkon qadar ijobiy hal qilishga harakat qilinadi. 
  
  —O'zbekistonga qaytarilgan fuqarolar maxsus dala shifoxonalariga joylashtirilishi aytilgan. Ayni paytda ular barcha qaytarilganlarni qabul qila oladimi? — Xorijdan charter reyslarda kelgan fuqarolarimiz 14 kun mobaynida karantin hududiga joylashtiriladi. Bunda joy yetishmovchiligi muammosi bo'lmaydi. Ilyos Safarov suhbatlashdi.`,
};
export const NewsData = [
  {
    src: require('./assets/imgs/newsImg2.png'),
    title: "Qirg'izistonda yana siyosiy inqiroz: Atamboyev nega taslim bo'ldi?",
    type: 'siyosat',
    time: 'Kecha 21:52',
  },
  {
    src: require('./assets/imgs/newsImg2.png'),
    title: "Qirg'izistonda yana siyosiy inqiroz: Atamboyev nega taslim bo'ldi?",
    type: 'siyosat',
    time: 'Kecha 21:52',
  },
  {
    src: require('./assets/imgs/newsImg3.png'),
    title: "Qirg'izistonda yana siyosiy inqiroz: Atamboyev nega taslim bo'ldi?",
    type: 'sport',
    time: 'Kecha 21:52',
  },
  {
    src: require('./assets/imgs/newsImg2.png'),
    title: "Qirg'izistonda yana siyosiy inqiroz: Atamboyev nega taslim bo'ldi?",
    type: 'siyosat',
    time: 'Kecha 21:52',
  },
  {
    src: require('./assets/imgs/newsImg3.png'),
    title: "Qirg'izistonda yana siyosiy inqiroz: Atamboyev nega taslim bo'ldi?",
    type: 'iqtisodiyot',
    time: '28-may 21:52',
  },
  {
    src: require('./assets/imgs/newsImg2.png'),
    title: "Qirg'izistonda yana siyosiy inqiroz: Atamboyev nega taslim bo'ldi?",
    type: 'siyosat',
    time: '5-april 21:52',
  },
  {
    src: require('./assets/imgs/newsImg2.png'),
    title: "Qirg'izistonda yana siyosiy inqiroz: Atamboyev nega taslim bo'ldi?",
    type: 'musiqa',
    time: 'Kecha 21:52',
  },
];

export const AboutDesc = `— O'zbekistonga qaytishni xohlagan xorijdagi fuqarolarning katta qismi Rossiya Federatsiyasi hissasiga to'g'ri keladi. E'lon qilingan parvozlar ro'yxatida Rossiyaning 8 ta shaharlaridan 11-24 may oralig'ida charter reyslar amalga oshiriladi. Albatta, bu 8 ta aviareyslar orqali barcha fuqarolarni olib kelishning imkoniyati yo'q. Buni fuqarolar to'g'ri tushunishlari lozim. Konsulliklar va elchixonalarimiz orqali qaytish istagida bo'lgan fuqarolarning ro'yxati shakllantirib borilyapti. Ro'yxatdan o'tgan barcha fuqarolarni charter reyslar orqali olib kelishga harakat qilinadi. Ulardan bizni to'g'ri tushungan holda sabrli bo'lishni so'raymiz.

— Qozog'iston chegarasida yuzlab o'zbekistonliklar o'z avtomashinalarida O'zbekistonga o'tish uchun ruxsat berilishini kutmoqda. Transport vazirligi bu masalada qo'shnilar bilan aloqa o'rnatdimi?

— Qozog'iston va Rossiya chegarasida taksi xizmatini ko'rsatuvchi bir necha yuzlab fuqarolarimiz to'planib qolgan va buni ijtimoiy tarmoqlarda videomurojaatlar orqali chiqishlarda bevosita guvohi bo'ldik. Bu bo'yicha  Transport vazirligi tegishli idoralar bilan hamkorlikda ushbu masalaga yechim topish ustida ishlayapti.  Bir jihatni alohida ta'kidlash kerak: ushbu fuqarolar Rossiya va Qozog'istonda belgilangan karantin talablarini qo'pol ravishda buzib harakatlanishmoqda. Qaytaraman, bu borada muzokaralar boshlandi.

— Rossiyada moddiy jihatdan qiyin ahvolga tushib qolgan va aviachipta uchun puli qolmagan vatandoshlarningO'zbekistonga qaytib olishlari uchun vazirlik tomonidan ko'mak beriladimi?

— O'zbekistonga qaytish istagida bo'lgan, ammo aviachipta xarid qilishga imkoniyati bo'lmagan fuqarolar masalasi individual tarzda o'rganiladi va imkon qadar ijobiy hal qilishga harakat qilinadi.

—O'zbekistonga qaytarilgan fuqarolar maxsus dala shifoxonalariga joylashtirilishi aytilgan. Ayni paytda ular barcha qaytarilganlarni qabul qila oladimi?

— Xorijdan charter reyslarda kelgan fuqarolarimiz 14 kun mobaynida karantin hududiga joylashtiriladi. Bunda joy yetishmovchiligi muammosi bo'lmaydi.

Ilyos Safarov suhbatlashdi.`;

export const TvChannelsData = [
  {
    title: 'markaziy studiya',
    bgColor: ['#DA1C5C', '#891750', '#4D1446'],
  },
  {
    title: 'milly tv',
    bgColor: ['#F9DD8F', '#F0B371', '#DE5E34'],
  },
  {
    title: 'discovery',
    bgColor: ['#EF8AB7', '#DB5B9C', '#CC3888'],
  },
  {
    title: 'cs50 tv',
    bgColor: ['#98DAF6', '#6EC6F2', '#98DAF6'],
  },
  {
    title: 'beeline tv',
    bgColor: ['#28A5EA', '#1E69A5', '#10103D'],
  },
  {
    title: 'markaziy studiya',
    bgColor: ['#DA1C5C', '#891750', '#4D1446'],
  },
  {
    title: 'milly tv',
    bgColor: ['#F9DD8F', '#F0B371', '#DE5E34'],
  },
  {
    title: 'discovery',
    bgColor: ['#EF8AB7', '#DB5B9C', '#CC3888'],
  },
  {
    title: 'cs50 tv',
    bgColor: ['#98DAF6', '#6EC6F2', '#98DAF6'],
  },
];

export const NewsCatergoryData = [
  {
    title: 'Madaniyat',
    bgColor: '#FFA4C145',
    color: '#E1084D',
    id: '557',
    isChecked: true,
  },
  {
    title: 'Madaniyat',
    bgColor: '#55C5F8',
    color: '#0071D4',
    id: '26',
    isChecked: true,
  },
  {
    title: 'Iqtisodiyot',
    bgColor: '#fcd1bd',
    color: '#FA6400',
    id: '54',
  },
  {
    title: 'Musiqa',
    bgColor: '#8adabf',
    color: '#126e4f',
    id: '53',
  },
  {
    title: 'Sport',
    bgColor: '#FFA4C145',
    color: '#E1084D',
    id: '55',
    isChecked: true,
  },
];

export const TvTimeData = [
  {
    time: '11:00',
    title: 'Biz bilan',
    id: '26',
  },
  {
    time: '14:00',
    title: 'Start Up Club',
    id: '54',
  },
  {
    time: '19:00',
    title: 'Musiqa',
    id: '53',
  },
  {
    time: '21:00',
    title: 'Retro Film',
    id: '55',
  },
  {
    time: '23:00',
    title: 'Nega kelding',
    id: '527',
  },
  {
    time: '23:20',
    title: 'Milliy musiqa',
    isPlaying: true,
    id: '5236',
  },
  {
    time: '10:00',
    title: 'Yoshlar maktabi',
    id: '556267',
  },
  {
    time: '11:00',
    title: 'Biz bilan',
    id: '2556',
  },
  {
    time: '14:00',
    title: 'Start Up Club',
    id: '55154',
  },
  {
    time: '19:00',
    title: 'Musiqa',
    id: '55453',
  },
  {
    time: '21:00',
    title: 'Retro Film',
    id: '555199',
  },
  {
    time: '22:00',
    title: "Otalar so'zi",
    id: '5577',
  },
  {
    time: '00:00',
    title: 'Kun News',
    id: '464557',
  },
  {
    time: '02:00',
    title: 'Yor-yor',
    id: '589157',
  },
  {
    time: '04:00',
    title: 'Rasmli multfilm',
    id: '5204857',
  },
  {
    time: '05:00',
    title: 'Stole qilingan tarjima kino',
    id: '2055557',
  },
  {
    time: '11:00',
    title: 'Biz bilan',
    id: '2106',
  },
  {
    time: '14:00',
    title: 'Start Up Club',
    id: '1054',
  },
  {
    time: '19:00',
    title: 'Musiqa',
    id: '5583',
  },
];

export const TvProgramsData = [
  {
    src: require('./assets/imgs/program1.png'),
    title: 'Markaziy studiya 839 soni',
    time: '09.01.2018 10:08',
  },
  {
    src: require('./assets/imgs/program2.png'),
    title: 'Markaziy studiya 839 soni',
    time: '09.01.2018 10:08',
  },
  {
    src: require('./assets/imgs/program1.png'),
    title: 'Markaziy studiya 839 soni',
    time: '09.01.2018 10:08',
  },
];
