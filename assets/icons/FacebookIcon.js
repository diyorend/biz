import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

function FacebookIcon(props) {
  return (
    <Svg
      width={7}
      height={15}
      viewBox="0 0 7 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}>
      <Path
        d="M4.375 5.154V3.281c0-.248.092-.487.256-.663a.846.846 0 01.619-.274h.875V0h-1.75C2.925 0 1.75 1.26 1.75 2.813v2.341H0v2.344h1.75V15h2.625V7.5h1.75L7 5.154H4.375z"
        fill="#2168EF"
      />
    </Svg>
  );
}

export default FacebookIcon;
