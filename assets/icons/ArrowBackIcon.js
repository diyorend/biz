import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

function ArrowBackIcon(props) {
  return (
    <Svg
      width={24}
      height={24}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}>
      <Path opacity={0.01} fill="#000" d="M0 0H24V24H0z" />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8.05 12.707a1 1 0 010-1.414l4.243-4.243a1 1 0 111.414 1.414L10.171 12l3.536 3.536a1 1 0 01-1.414 1.414L8.05 12.707z"
        fill="#1E1F20"
      />
    </Svg>
  );
}

export default ArrowBackIcon;
