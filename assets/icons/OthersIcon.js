import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

function OthersIcon(props) {
  return (
    <Svg
      width={18}
      height={6}
      viewBox="0 0 18 6"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}>
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.062 3.462a2.531 2.531 0 11-5.062 0 2.531 2.531 0 015.062 0zm6.328 0a2.531 2.531 0 11-5.062 0 2.531 2.531 0 015.062 0zm3.796 2.53a2.531 2.531 0 100-5.061 2.531 2.531 0 000 5.062z"
        fill={props.fill}
        fillOpacity={0.794034}
      />
    </Svg>
  );
}

export default OthersIcon;
