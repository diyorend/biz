import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

function LocationIcon(props) {
  return (
    <Svg
      width={props.w || 20}
      height={props.h || 20}
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}>
      <Path
        d="M14.039 14.336a.333.333 0 00-.078.662c3.505.412 5.372 1.524 5.372 2.169 0 .904-3.55 2.166-9.333 2.166S.667 18.071.667 17.167c0-.645 1.867-1.757 5.372-2.17a.334.334 0 00-.078-.661C2.451 14.749 0 15.913 0 17.166 0 18.576 3.435 20 10 20c6.565 0 10-1.425 10-2.833 0-1.254-2.451-2.418-5.961-2.831z"
        fill="#FFCC57"
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M15.055 10.867L9.94 18.252 4.82 10.856c-1.91-2.547-1.633-6.75.601-8.984A6.35 6.35 0 019.941 0a6.35 6.35 0 014.52 1.873c2.234 2.234 2.512 6.437.594 8.994zm-2.721-4.534A2.336 2.336 0 0010 4a2.336 2.336 0 00-2.333 2.333A2.336 2.336 0 0010 8.667a2.336 2.336 0 002.334-2.334z"
        fill="#FFA4C1"
      />
    </Svg>
  );
}

export default LocationIcon;
