import * as React from 'react';
import Svg, {Mask, Path, G} from 'react-native-svg';

function LikeIcon(props) {
  return (
    <Svg
      width={props.w || 13}
      height={props.h || 13}
      viewBox="0 0 13 13"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}>
      <Mask
        id="a"
        style={{
          maskType: 'alpha',
        }}
        maskUnits="userSpaceOnUse"
        x={0}
        y={0}
        width={13}
        height={13}>
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M.117.831h12.182v11.312H.117V.83z"
          fill="#fff"
        />
      </Mask>
      <G
        mask="url(#a)"
        fillRule="evenodd"
        clipRule="evenodd"
        fill={props.color || '#0071D4'}>
        <Path d="M2.292 4.312H.552c-.24 0-.435.175-.435.391v7.048c0 .216.195.392.435.392h1.74c.24 0 .435-.176.435-.392V4.703c0-.216-.196-.391-.435-.391zM8.377 4.707l.405-1.187c.28-.82.099-2.072-.676-2.517-.253-.145-.606-.218-.89-.14a.648.648 0 00-.392.315c-.098.179-.088.387-.123.585-.089.503-.31.981-.652 1.34-.597.625-2.452 2.43-2.452 2.43v6.61h6.409c.864 0 1.431-1.037 1.005-1.848.508-.35.682-1.086.384-1.652.509-.35.683-1.087.385-1.652.877-.604.584-2.098-.441-2.284-.062-.011-2.962 0-2.962 0z" />
      </G>
    </Svg>
  );
}

export default LikeIcon;
