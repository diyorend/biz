import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

function GoogleIcon(props) {
  return (
    <Svg
      width={23}
      height={14}
      viewBox="0 0 23 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}>
      <Path
        d="M20.146 5.764V3h-1.382v2.764H16v1.382h2.764v2.765h1.382V7.146h2.765V5.764h-2.765zM6.91 5.528v2.765h3.91a4.146 4.146 0 11-3.91-5.529 4.085 4.085 0 012.686 1.001l1.816-2.084A6.848 6.848 0 006.911 0a6.91 6.91 0 106.91 6.91V5.529h-6.91z"
        fill="#FF3B50"
      />
    </Svg>
  );
}

export default GoogleIcon;
