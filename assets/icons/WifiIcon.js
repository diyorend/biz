import * as React from 'react';
import Svg, {Circle, Path} from 'react-native-svg';

function WifiIcon(props) {
  return (
    <Svg
      width={12}
      height={13}
      viewBox="0 0 12 13"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}>
      <Circle cx={1.8024} cy={10.8024} r={1.8024} fill="#FF8519" />
      <Path
        d="M0 4v2.182a5.467 5.467 0 015.462 5.46h2.182A7.655 7.655 0 000 4z"
        fill="#FF8519"
      />
      <Path
        d="M0 0v2.25a9.584 9.584 0 019.575 9.578h2.25C11.818 5.299 6.528.008 0 0z"
        fill="#FF8519"
      />
    </Svg>
  );
}

export default WifiIcon;
