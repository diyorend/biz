import * as React from 'react';
import Svg, {Path, Circle} from 'react-native-svg';

function InstagramIcon(props) {
  return (
    <Svg
      width={18}
      height={18}
      viewBox="0 0 18 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}>
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.381 0h6.458c2.972 0 5.38 2.41 5.381 5.381v6.458c0 2.972-2.41 5.38-5.381 5.381H5.38A5.382 5.382 0 010 11.839V5.38C0 2.41 2.41.001 5.381 0zm6.458 15.606a3.771 3.771 0 003.767-3.767V5.38a3.771 3.771 0 00-3.767-3.767H5.38a3.771 3.771 0 00-3.767 3.767v6.458a3.771 3.771 0 003.767 3.767h6.458z"
        fill="#FF3C8C"
      />
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M4.5 8.609a4.109 4.109 0 118.218 0 4.109 4.109 0 01-8.218 0zm1.54 0a2.568 2.568 0 105.137 0 2.568 2.568 0 00-5.136 0z"
        fill="#FF3C8C"
      />
      <Circle cx={13.669} cy={3.669} r={0.669} fill="#FF3C8C" />
    </Svg>
  );
}

export default InstagramIcon;
