import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

function TvIcon(props) {
  return (
    <Svg
      width={24}
      height={22}
      viewBox="0 0 24 22"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}>
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M21.081 4.916h-8.066L17.1.922l-.738-.73-4.245 4.124L7.874.166l-.738.73 4.086 3.994H2.365c-1.16 0-2.11.94-2.11 2.088v12.27c0 1.148.95 2.088 2.11 2.088H21.08c1.16 0 2.11-.94 2.11-2.089V6.978c0-1.122-.95-2.062-2.11-2.062zm-6.59 11.642c0 .862-.712 1.567-1.582 1.567H4.737c-.87 0-1.582-.705-1.582-1.567V9.771c0-.861.712-1.566 1.582-1.566h8.172c.87 0 1.582.705 1.582 1.566v6.787zm2.109-.887c0 1.018.817 1.827 1.845 1.827s1.846-.809 1.846-1.827a1.825 1.825 0 00-1.846-1.827c-1.028 0-1.845.809-1.845 1.827zm3.954-3.472h-4.218v-1.044h4.218v1.044zm-4.218-2.61h4.218V8.545h-4.218v1.044z"
        fill={props.fill}
        fillOpacity={0.794034}
      />
    </Svg>
  );
}

export default TvIcon;
