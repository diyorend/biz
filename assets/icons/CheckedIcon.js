import * as React from 'react';
import Svg, {Path} from 'react-native-svg';

function CheckedIcon(props) {
  return (
    <Svg
      width={props.width || 14}
      height={props.height || 14}
      viewBox="0 0 14 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}>
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M1.545 1.482C3.13-.123 8.046-.424 10.877.577l.004.015c.66.23 1.206.532 1.568.902 1.922 1.965 2.14 9.432 0 11.179-2.14 1.746-8.82 1.746-10.916 0C.178 11.566-.23 8.165.14 5.32c.192-1.502.604-2.849 1.22-3.626a2.513 2.513 0 01.186-.212z"
        fill={props.color || '#E1084D'}
      />
      <Path
        d="M5.987 10.882c-.306 0-.611-.13-.786-.393L2.756 7.345c-.35-.436-.262-1.047.174-1.397.437-.35 1.048-.262 1.398.175l1.659 2.14 3.668-4.76c.35-.437.96-.524 1.397-.175.437.35.524.961.175 1.398l-4.454 5.763c-.218.219-.48.393-.786.393z"
        fill="#fff"
      />
    </Svg>
  );
}

export default CheckedIcon;
