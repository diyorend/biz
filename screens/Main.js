import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
//Screens
import TvSreen from './TvSreen';
import TvIntoScreen from './TvIntoScreen';
import NewsSreen from './NewsSreen';
import NewsIntoScreen from './NewsIntoScreen';
import ProgramsSreen from './ProgramsSreen';
import ProgramsIntoScreen from './ProgramsIntoScreen';
import OthersSreen from './OthersSreen';
import OthersTvProgramsScreen from './OthersTvProgramsScreen';
import OthersAboutScreen from './OthersAboutScreen';

//Icons
import TvIcon from '../assets/icons/TvIcon';
import NewsIcon from '../assets/icons/NewsIcon';
import ProgramsIcon from '../assets/icons/ProgramsIcon';
import OthersIcon from '../assets/icons/OthersIcon';
import {SafeAreaView} from 'react-native';

//screen names
const TvName = 'Biz TV';
const TvIntoName = 'TvInto';
const NewsIntoName = 'NewsInto';
const NewsName = 'News';
const ProgramsName = 'Programs';
const ProgramsIntoName = 'ProgramsInto';
const OthersName = 'Others';
const OthersTvProgramsName = 'OthersTv';
const OthersAboutName = 'OthersAbout';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const TvHome = () => {
  return (
    <NavigationContainer independent={true}>
      <Stack.Navigator
        initialRouteName={TvName}
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name={TvName} component={TvSreen} />
        <Stack.Screen name={TvIntoName} component={TvIntoScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
const NewsHome = () => {
  return (
    <NavigationContainer independent={true}>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name={NewsName} component={NewsSreen} />
        <Stack.Screen name={NewsIntoName} component={NewsIntoScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const ProgramsHome = () => {
  return (
    <NavigationContainer independent={true}>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name={ProgramsName} component={ProgramsSreen} />
        <Stack.Screen name={ProgramsIntoName} component={ProgramsIntoScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const OthersHome = () => {
  return (
    <NavigationContainer independent={true}>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name={OthersName} component={OthersSreen} />
        <Stack.Screen
          name={OthersTvProgramsName}
          component={OthersTvProgramsScreen}
        />
        <Stack.Screen name={OthersAboutName} component={OthersAboutScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
const Tabs = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        headerShown: false,
        tabBarIcon: ({color}) => {
          const routeName = route.name;

          if (routeName == TvName) {
            return <TvIcon fill={color} />;
          } else if (routeName == NewsName) {
            return <NewsIcon fill={color} />;
          } else if (routeName == ProgramsName) {
            return <ProgramsIcon fill={color} />;
          } else {
            return <OthersIcon fill={color} />;
          }
        },
        tabBarActiveTintColor: '#E1084D',
        tabBarInactiveTintColor: '#75758C',
        tabBarLabelStyle: {paddingBottom: 10, fontSize: 12},
        tabBarStyle: {padding: 10, height: 70},
      })}>
      <Tab.Screen name={TvName} component={TvHome} />
      <Tab.Screen name={NewsName} component={NewsHome} />
      <Tab.Screen name={ProgramsName} component={ProgramsHome} />
      <Tab.Screen name={OthersName} component={OthersHome} />
    </Tab.Navigator>
  );
};

const Main = () => {
  return (
    <NavigationContainer>
      <Tabs />
    </NavigationContainer>
  );
};

export default Main;
