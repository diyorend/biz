import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import React from 'react';
import {BackButton} from '../components/Buttons';
import {NewsInfoData} from '../Data';

const NewsIntoScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <BackButton
        marginX={15}
        marginY={15}
        handlePress={() => navigation.goBack()}
      />
      <ScrollView>
        <View style={styles.imgView}>
          <Image style={styles.img} source={NewsInfoData.src} />
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.title}>{NewsInfoData.title}</Text>
          <Text style={styles.description}>{NewsInfoData.desc}</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },
  imgView: {
    flex: 1,
    alignItems: 'center',
  },
  img: {
    width: '100%',
    resizeMode: 'cover',
  },
  textContainer: {
    paddingVertical: 20,
    marginHorizontal: 10,
  },
  title: {
    paddingVertical: 15,
    fontSize: 20,
    fontWeight: '600',
    color: '#253536',
  },
  description: {
    fontSize: 16,
    color: '#000',
  },
});

export default NewsIntoScreen;
