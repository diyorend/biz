import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Switch,
  Pressable,
} from 'react-native';
import React, {useState} from 'react';

// components
import OthersLinks from '../components/OthersLinks';
import OthersCard from '../components/OthersCard';

//Icons
const TvLogo = require('../assets/imgs/BiztvOthers.png');
import LocationIcon from '../assets/icons/LocationIcon';
import LoudIcon from '../assets/icons/LoudIcon';

const OthersSreen = ({navigation}) => {
  const [isOn, setIsOn] = useState(false);
  const toggleSwitch = () => setIsOn(prev => !prev);

  return (
    <View style={styles.container}>
      <View style={{marginHorizontal: 50}}>
        <OthersLinks />
      </View>
      <View style={{marginVertical: 20, marginHorizontal: 20}}>
        <OthersCard
          handlePress={() => navigation.navigate('OthersTv')}
          imgSrc={TvLogo}
          title="Teledasturlar"
        />
        <OthersCard svg={<LoudIcon />} title="Reklama" />
        <OthersCard svg={<LocationIcon />} title="Biz bilan aloqa" />
      </View>

      <View style={styles.faqCard}>
        <TouchableOpacity
          onPress={() => navigation.navigate('OthersAbout')}
          style={styles.faqCardItem}>
          <Text style={styles.text}>Loyiha haqida</Text>
        </TouchableOpacity>
        <View style={styles.faqCardLine}></View>
        <TouchableOpacity style={styles.faqCardItem}>
          <Text style={styles.text}>Foydalanish shartlari</Text>
        </TouchableOpacity>
        <View style={styles.faqCardLine}></View>
        <Pressable
          style={[
            styles.faqCardItem,
            {
              flexDirection: 'row',
              justifyContent: 'space-between',
            },
          ]}
          onPress={toggleSwitch}>
          <Text style={styles.text}>Bildirishnomalar</Text>
          <Switch
            trackColor={{false: '#767577', true: 'rgba(255, 164, 193, 1)'}}
            thumbColor={isOn ? '#fff' : '#f4f3f4'}
            onValueChange={toggleSwitch}
            value={isOn}
          />
        </Pressable>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  text: {
    color: 'rgba(8, 0, 64, 1)',
    fontSize: 16,
    fontWeight: '500',
  },
  faqCard: {
    backgroundColor: '#fff',
    marginHorizontal: 20,
    borderRadius: 15,
  },
  faqCardItem: {
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  faqCardLine: {
    paddingVertical: 0.8,
    backgroundColor: 'rgba(239, 244, 251, 1)',
    marginHorizontal: 20,
  },
});

export default OthersSreen;
