import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import React from 'react';
//components
import NewsTabList from '../components/NewsTabList';
//tailwind css
import tw from 'twrnc';
//Data
import {NewsData} from '../Data';

const NewsSreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      {/* TabList */}
      <SafeAreaView>
        <NewsTabList />
      </SafeAreaView>

      <ScrollView
        style={styles.scrollContainer}
        showsVerticalScrollIndicator={false}>
        {/* Main News */}
        <TouchableOpacity onPress={() => navigation.navigate('NewsInto')}>
          <Image
            source={require('../assets/imgs/newsImg1.png')}
            style={styles.mainImg}
          />
          <Text style={styles.mainTitle}>
            Qirg'izistonda yana siyosiy inqiroz: Atamboyev nega taslim bo'ldi?
          </Text>
          <View style={styles.mainInfo}>
            <Text
              style={tw`px-2 py-1  bg-[#8adabf] text-[#1FB884] font-bold rounded-lg`}>
              Siyosat
            </Text>
            <Text
              style={tw`px-2 py-1 text-gray-400 font-bold rounded-lg uppercase`}>
              Kecha 21:06
            </Text>
          </View>
        </TouchableOpacity>

        {/* News List */}
        <View>
          {NewsData.map(({time, title, type, src}, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => navigation.navigate('NewsInto')}
              style={tw`flex-row  my-2 justify-start`}>
              <View style={tw`flex-1`}>
                <Image source={src} style={tw`w-full  rounded-lg`} />
              </View>
              <View style={tw`mx-3 w-full justify-between flex-3`}>
                <Text style={tw`font-medium text-[15px] mb-2`}>{title}</Text>
                <View style={tw`flex-row`}>
                  <Text
                    style={tw`px-2 py-1  bg-[#8adabf] text-[#1FB884] font-bold rounded-lg capitalize`}>
                    {type}
                  </Text>
                  <Text
                    style={tw`px-2 py-1 text-gray-400 font-bold rounded-lg uppercase`}>
                    {time}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          ))}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollContainer: {
    marginHorizontal: 20,
  },
  mainImg: {
    width: '100%',
    borderRadius: 15,
  },
  mainTitle: {
    fontSize: 16,
    fontWeight: '600',
    color: '#253536',
    paddingVertical: 10,
  },
  mainInfo: {
    flexDirection: 'row',
    paddingBottom: 10,
  },
});
export default NewsSreen;
