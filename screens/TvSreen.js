import {View, Text, StyleSheet, ScrollView} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';
//Components
import Logos from '../components/Logos';
//src of imgs
const TvLogo = require('../assets/imgs/BizTvLogo.png');
const CinemaLogo = require('../assets/imgs/BizCinemaLogo.png');
const MusicLogo = require('../assets/imgs/BizMusicLogo.png');

const TvSreen = () => {
  const navigation = useNavigation();

  return (
    <ScrollView
      contentContainerStyle={{
        justifyContent: 'center',
        alignItems: 'center',
        top: 30,
      }}
      style={styles.container}>
      <View style={styles.textContainer}>
        <Text onPress={() => navigation.navigate('TvInto')} style={styles.text}>
          Assalomu alaykum, iltimos bizning kanallaridan birini tanlang
        </Text>
      </View>

      <Logos
        onPress={() => navigation.navigate('TvInto')}
        src={TvLogo}
        bgColor="rgba(255, 76, 152, 1)"
      />
      <Logos src={CinemaLogo} bgColor="rgba(18, 36, 94, 1)" />
      <Logos src={MusicLogo} bgColor="rgba(109, 212, 0, 1)" />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textContainer: {
    width: '90%',
  },
  text: {
    fontSize: 18,
    fontWeight: '400',
    color: '#000',
  },
});

export default TvSreen;
