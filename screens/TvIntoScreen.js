import {View} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';
//components
import {BackButton} from '../components/Buttons';
import Segment from '../components/Segment';
import ProgramsList from '../components/ProgramsList';

const TvIntoScreen = () => {
  const navigation = useNavigation();
  return (
    <>
      <BackButton
        marginX={15}
        marginY={15}
        handlePress={() => navigation.goBack()}
        position={'relative'}
      />

      <View style={{marginVertical: 20, marginHorizontal: 20}}>
        <Segment />
      </View>

      <ProgramsList />
    </>
  );
};

export default TvIntoScreen;
