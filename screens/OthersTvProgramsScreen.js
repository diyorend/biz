import {View, Text, StyleSheet} from 'react-native';
import React from 'react';
//compenents
import {BackButton} from '../components/Buttons';
import ProgramsList from '../components/ProgramsList';

const OthersTvProgramsScreen = ({navigation}) => {
  return (
    <>
      <View style={styles.container}>
        <BackButton
          handlePress={() => navigation.goBack()}
          position={'relative'}
          bgColor="rgba(255, 164, 193, 0.27)"
        />
        <Text style={styles.tabTitle}>Teledasturlar</Text>
      </View>
      <ProgramsList />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  tabTitle: {
    flex: 1,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '600',
    color: 'rgba(37, 53, 54, 1)',
  },
});

export default OthersTvProgramsScreen;
