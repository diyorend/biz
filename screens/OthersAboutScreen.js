import React from 'react';
import {SafeAreaView, ScrollView, Text, View} from 'react-native';
//components
import TabWithTitle from '../components/TabWithTitle';
//Data
import {AboutDesc} from '../Data';

const OthersAboutScreen = ({navigation}) => {
  return (
    <SafeAreaView style={{flex: 1}}>
      {/* Tab */}
      <TabWithTitle
        handlePress={() => navigation.goBack()}
        position={'relative'}
        bgColor="rgba(255, 164, 193, 0.27)"
        title="Loyiha haqida"
      />

      <ScrollView>
        <View style={{flex: 1, alignItems: 'center', marginHorizontal: 15}}>
          <Text style={{paddingVertical: 30, fontSize: 16, color: '#111'}}>
            {AboutDesc}
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default OthersAboutScreen;
