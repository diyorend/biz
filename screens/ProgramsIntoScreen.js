import {SafeAreaView} from 'react-native';
import React from 'react';
// components
import ProgramsScrollData from '../components/ProgramsScrollData';
import TabWithTitle from '../components/TabWithTitle';

const ProgramsIntoScreen = ({navigation}) => {
  return (
    <SafeAreaView style={{flex: 1}}>
      <TabWithTitle
        handlePress={() => navigation.goBack()}
        position={'relative'}
        bgColor="rgba(255, 164, 193, 0.27)"
        title="TV Programs"
      />

      <ProgramsScrollData />
    </SafeAreaView>
  );
};

export default ProgramsIntoScreen;
