import {useWindowDimensions, ScrollView, TouchableOpacity} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';
// Tab View Pack
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
// Components
import ChannelCard from '../components/ChannelCard';
import ProgramsScrollData from '../components/ProgramsScrollData';
//Data
import {TvChannelsData} from '../Data';

const FirstRoute = () => {
  return <ProgramsScrollData />;
};

const SecondRoute = () => {
  const navigation = useNavigation();
  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      contentContainerStyle={{
        paddingHorizontal: 15,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
      }}>
      {TvChannelsData.map(({title, bgColor}, index) => (
        <TouchableOpacity
          onPress={() => navigation.navigate('ProgramsInto')}
          key={index}>
          <ChannelCard title={title} gradientColors={bgColor} />
        </TouchableOpacity>
      ))}
    </ScrollView>
  );
};
const renderScene = SceneMap({
  first: FirstRoute,
  second: SecondRoute,
});

const ProgramsSreen = () => {
  const layout = useWindowDimensions();
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'Barchasi'},
    {key: 'second', title: "Ko'rsatuvlar"},
  ]);

  return (
    <TabView
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{width: layout.width}}
      renderTabBar={props => (
        <TabBar
          inactiveColor="rgba(0, 0, 0, 0.5)"
          activeColor="rgba(225, 8, 77, 1)"
          indicatorStyle={{
            height: 3,
            backgroundColor: 'rgba(225, 8, 77, 1)',
            borderRadius: 5,
            width: '45%',
            marginHorizontal: 10,
          }}
          labelStyle={{
            fontWeight: '600',
            textTransform: 'capitalize',
            fontSize: 15,
            paddingVertical: 7,
          }}
          style={{
            backgroundColor: '#fff',
          }}
          {...props}
        />
      )}
    />
  );
};

export default ProgramsSreen;
