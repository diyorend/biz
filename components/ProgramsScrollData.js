import {
  View,
  Text,
  ScrollView,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';
//tailwind css
import tw from 'twrnc';

//components
import {LikeButton, ShareButton} from '../components/Buttons';
import {PlayButton} from '../components/Buttons';

//data
import {TvProgramsData} from '../Data';

//ko'rsatuvlar listi
const ProgramsScrollData = () => {
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      {TvProgramsData.map(({src, time, title}, index) => {
        const [likes, setLikes] = useState(0);
        const [liked, setLiked] = useState(false);
        return (
          <View key={index} style={tw`mx-3 my-5 pt-2`}>
            <View
              style={{
                position: 'relative',
                justifyContent: 'center',
                alignItems: 'center',
                height: 200,
              }}>
              <TouchableOpacity
                style={{
                  zIndex: 1,
                }}>
                <PlayButton />
              </TouchableOpacity>
              <Image
                style={{position: 'absolute', borderRadius: 10, width: '100%'}}
                source={src}
              />
            </View>
            <Text style={tw`text-[18px] font-medium py-2 text-[#2945AB]`}>
              {title}
            </Text>
            <View style={tw`flex-row justify-between`}>
              <View style={tw`flex-row items-center flex-2 justify-between`}>
                <ShareButton w={16} h={16} />
                <LikeButton
                  w={16}
                  h={16}
                  handleLike={() => {
                    liked
                      ? setLikes(prev => prev - 1)
                      : setLikes(prev => prev + 1);
                    setLiked(prev => !prev);
                  }}
                  color={liked ? 'rgba(0,0,0,1)' : '#0071D4'}
                  bgColor={liked ? 'rgba(255,255,255,1)' : '#32C5FF42'}
                />
                <Text>{likes}</Text>
              </View>
              <View style={tw`flex-6 items-end justify-center`}>
                <Text>{time}</Text>
              </View>
            </View>
          </View>
        );
      })}
    </ScrollView>
  );
};

export default ProgramsScrollData;
