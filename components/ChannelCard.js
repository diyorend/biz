import {Text} from 'react-native';
import React from 'react';
//tailwind css
import tw from 'twrnc';
import LinearGradient from 'react-native-linear-gradient';

const ChannelCard = ({title, gradientColors}) => {
  return (
    <LinearGradient
      start={{x: 0.5, y: 0.25}}
      end={{x: 0, y: 1.0}}
      colors={gradientColors}
      style={tw`my-3 mx-2 h-50 w-40 items-start justify-end rounded-lg`}>
      <Text style={tw`uppercase text-[24px] font-medium px-3 py-5 text-[#fff]`}>
        {title}
      </Text>
    </LinearGradient>
  );
};

export default ChannelCard;
