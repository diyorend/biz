import {View, StyleSheet} from 'react-native';
import React, {useState} from 'react';
import SegmentedControlTab from 'react-native-segmented-control-tab';
//component
import Card from './Card';

// TvInto Screendagi 480-720-1080
const Segment = () => {
  const [customSelectedIndex, setCustomSelectedIndex] = useState(0);

  const updateCustomSegment = index => {
    setCustomSelectedIndex(index);
  };
  return (
    <View>
      {customSelectedIndex === 0 && <Card quality={480} />}
      {customSelectedIndex === 1 && <Card quality={720} />}
      {customSelectedIndex === 2 && <Card quality={1080} />}
      <SegmentedControlTab
        borderRadius={40}
        values={['480', '720', '1080']}
        selectedIndex={customSelectedIndex}
        onTabPress={updateCustomSegment}
        tabsContainerStyle={{
          height: 50,
          backgroundColor: 'rgba(213, 221, 237, 0.5857)',
          borderRadius: 20,
        }}
        tabStyle={{
          backgroundColor: 'transparent',
          marginVertical: 4,
          borderColor: 'transparent',
          marginHorizontal: 5,
        }}
        activeTabStyle={{
          backgroundColor: '#fff',
          borderRadius: 20,
        }}
        tabTextStyle={{
          color: 'rgba(117, 117, 140, 1)',
          fontWeight: 'bold',
          fontSize: 16,
        }}
        activeTabTextStyle={{
          color: '#000',
          fontSize: 16,
          color: 'rgba(225, 8, 77, 1)',
        }}
      />
    </View>
  );
};

export default Segment;
