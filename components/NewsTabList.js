import {Text, FlatList, TouchableOpacity} from 'react-native';
import React from 'react';
//tailwind
import tw from 'twrnc';
//icons
import CheckedIcon from '../assets/icons/CheckedIcon';
//data
import {NewsCatergoryData} from '../Data';

const Item = ({title, isChecked, bgColor, color}) => {
  return (
    <TouchableOpacity
      style={tw`mx-2 my-3 justify-start flex-row px-3 py-2 items-center bg-[${bgColor}] rounded-lg`}>
      <Text style={tw`text-[${color}] font-medium text-[16px] mr-3`}>
        {title}
      </Text>
      {isChecked && <CheckedIcon color={color} />}
    </TouchableOpacity>
  );
};

const NewsTabList = () => {
  const renderItem = ({item}) => (
    <Item
      title={item.title}
      isChecked={item.isChecked}
      bgColor={item.bgColor}
      color={item.color}
    />
  );

  return (
    <FlatList
      showsHorizontalScrollIndicator={false}
      horizontal={true}
      data={NewsCatergoryData}
      renderItem={renderItem}
      keyExtractor={item => item.id}
    />
  );
};
export default NewsTabList;
