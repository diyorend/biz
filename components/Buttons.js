import {TouchableOpacity, View} from 'react-native';
import React from 'react';

// Icons
import LikeIcon from '../assets/icons/LikeIcon';
import ShareIcon from '../assets/icons/ShareIcon';
import BackIcon from '../assets/icons/BackIcon';
import PlayTvIcon from '../assets/icons/PlayTvIcon';

export const BackButton = ({
  handlePress,
  marginX,
  marginY,
  bgColor,
  position,
}) => {
  return (
    <TouchableOpacity
      style={{
        marginHorizontal: marginX,
        marginVertical: marginY,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
        height: 40,
        width: 40,
        borderRadius: 5,
        backgroundColor: bgColor || 'rgba(255, 255, 255, 0.7)',
        position: position || 'absolute',
        zIndex: 1,
      }}
      onPress={handlePress}>
      <BackIcon />
    </TouchableOpacity>
  );
};

export const LikeButton = ({handleLike, w, h, color, bgColor}) => {
  return (
    <TouchableOpacity
      onPress={handleLike}
      style={{
        padding: 5,
        backgroundColor: bgColor,
        height: 30,
        width: 30,
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
        borderRadius: 25,
      }}>
      <LikeIcon h={h} w={w} color={color} />
    </TouchableOpacity>
  );
};

export const ShareButton = ({w, h}) => {
  return (
    <TouchableOpacity
      style={{
        padding: 5,
        backgroundColor: '#32C5FF42',
        height: 30,
        width: 30,
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
        borderRadius: 25,
      }}>
      <ShareIcon h={h} w={w} />
    </TouchableOpacity>
  );
};

export const OthersLinkButton = ({component}) => {
  return (
    <TouchableOpacity
      style={{
        padding: 5,
        backgroundColor: '#fff',
        height: 41,
        width: 50,
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
        borderRadius: 23,
        marginHorizontal: 3,
      }}>
      {component}
    </TouchableOpacity>
  );
};

export const PlayButton = () => (
  <TouchableOpacity
    style={{
      backgroundColor: 'rgba(2, 184, 234, 0.28)',
      height: 120,
      width: 120,
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex',
      borderRadius: 100,
    }}>
    <View
      style={{
        backgroundColor: 'rgba(2, 184, 234, 0.28)',
        height: 100,
        width: 100,
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
        borderRadius: 100,
      }}>
      <View
        style={{
          backgroundColor: '#fff',
          height: 80,
          width: 80,
          alignItems: 'center',
          justifyContent: 'center',
          display: 'flex',
          borderRadius: 100,
        }}>
        <PlayTvIcon />
      </View>
    </View>
  </TouchableOpacity>
);
