import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import React from 'react';

//others menusidagi Card
const OthersCard = ({svg, title, imgSrc, handlePress}) => {
  return (
    <TouchableOpacity onPress={handlePress} style={styles.container}>
      <View style={{flex: 1}}>{imgSrc ? <Image source={imgSrc} /> : svg}</View>
      <Text style={styles.titleText}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: '#fff',
    marginVertical: 7,
    borderRadius: 10,
    alignItems: 'center',
  },
  titleText: {
    flex: 7,
    color: 'rgba(8, 0, 64, 1)',
    fontWeight: '500',
    fontSize: 16,
  },
});
export default OthersCard;
