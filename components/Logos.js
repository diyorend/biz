import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import React from 'react';
// line:27
const TvLogo = require('../assets/imgs/BizTvLogo.png');

const Logos = ({src, bgColor, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <View
        style={{
          backgroundColor: bgColor,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: 20,
          height: '100%',
          width: '100%',
        }}>
        <Image
          source={src}
          style={src === TvLogo ? styles.tvImg : styles.bigImg}
        />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    width: '90%',
    height: 150,
  },
  tvImg: {
    width: '60%',
    height: 80,
    resizeMode: 'contain',
  },
  bigImg: {
    height: 150,
    width: '70%',
    resizeMode: 'contain',
  },
});

export default Logos;
