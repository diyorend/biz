import {View, Text, FlatList, TouchableOpacity} from 'react-native';
import React from 'react';
// tailwind css
import tw from 'twrnc';
//data
import {TvTimeData} from '../Data';

const Item = ({time, title, isPlaying}) => {
  return (
    <TouchableOpacity
      style={tw`mx-2 justify-start flex-row py-2 items-center ${
        isPlaying && 'bg-[#FED4D459] py-5 rounded-md'
      }`}>
      <Text style={tw`mx-3 text-gray-400 font-semibold `}>{time}</Text>
      <View style={tw`mx-3 justify-between`}>
        <Text style={tw`text-[#253536] font-medium text-[16px]`}>{title}</Text>
        {isPlaying && (
          <Text
            style={tw`mt-2 text-[#FF4B6A] uppercase font-medium text-[16px]`}>
            • Now Playing
          </Text>
        )}
      </View>
    </TouchableOpacity>
  );
};

const ProgramsList = ({}) => {
  const renderItem = ({item}) => (
    <Item title={item.title} time={item.time} isPlaying={item.isPlaying} />
  );

  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      data={TvTimeData}
      renderItem={renderItem}
      keyExtractor={item => item.id}
    />
  );
};

export default ProgramsList;
