import {View, StyleSheet} from 'react-native';
import React from 'react';

//icons
import FacebookIcon from '../assets/icons/FacebookIcon';
import InstagramIcon from '../assets/icons/InstagramIcon';
import GoogleIcon from '../assets/icons/GoogleIcon';
import TwitterIcon from '../assets/icons/TwitterIcon';
import WifiIcon from '../assets/icons/WifiIcon';
// button
import {OthersLinkButton} from './Buttons';

//others screen's social part
const OthersLinks = () => {
  return (
    <View style={styles.container}>
      <OthersLinkButton component={<FacebookIcon />} />
      <OthersLinkButton component={<GoogleIcon />} />
      <OthersLinkButton component={<TwitterIcon />} />
      <OthersLinkButton component={<InstagramIcon />} />
      <OthersLinkButton component={<WifiIcon />} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'rgba(218, 220, 224, 1)',
    alignItems: 'center',
    padding: 5,
    borderRadius: 30,
  },
});
export default OthersLinks;
