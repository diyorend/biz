import {View, Text, StyleSheet} from 'react-native';
import React from 'react';

//component
import {BackButton} from '../components/Buttons';

//Tab
const TabWithTitle = ({title, handlePress, position, bgColor}) => {
  return (
    <View style={styles.container}>
      <BackButton
        handlePress={handlePress}
        position={position}
        bgColor={bgColor}
      />
      <Text style={styles.titleText}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  titleText: {
    flex: 1,
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '600',
    color: 'rgba(37, 53, 54, 1)',
  },
});

export default TabWithTitle;
