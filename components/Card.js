import {View, Text, Image, StyleSheet} from 'react-native';
import React from 'react';
//tailwind css
import tw from 'twrnc';
// component
import {PlayButton} from './Buttons';

const Card = props => {
  return (
    <View style={styles.container}>
      <View style={styles.video}>
        <PlayButton />
        <Image
          source={require('../assets/imgs/biztvinto.png')}
          style={styles.videoImg}
        />
      </View>

      <View style={tw`items-center my-2 w-full bg-white `}>
        <Image source={require('../assets/imgs/uzb24tv.png')} />
      </View>

      <Text
        style={tw`absolute p-2 m-2 right-0 bg-white text-yellow-500 font-bold rounded-lg `}>
        {props.quality}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    shadowColor: '#000',
    shadowOpacity: 0.2,
    shadowRadius: 20,
    shadowOffset: {
      width: 15,
      height: 30,
    },
    elevation: 60,
    position: 'relative',
    alignItems: 'center',
    borderRadius: 10,
    overflow: 'hidden',
    width: '100%',
    marginBottom: 20,
    backgroundColor: '#fff',
  },
  video: {
    height: 200,
    width: '100%',
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
  },
  videoImg: {
    position: 'absolute',
    zIndex: -1,
    width: '100%',
    resizeMode: 'contain',
  },
});
export default Card;
