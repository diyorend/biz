import {View, Text} from 'react-native';
import React from 'react';
import Main from './screens/Main';

const App = () => {
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <Main />
    </View>
  );
};

export default App;
